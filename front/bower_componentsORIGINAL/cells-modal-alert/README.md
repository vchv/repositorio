![Component screenshot](cells-modal-alert.png)

# cells-modal-alert

<!--
[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

[Demo of component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/cells-modal-alert)
-->

`<cells-modal-alert>` is a bottom-modal to display an alert or warning message with a optional icon, text and none, one or two buttons.
The modal can also behave like a toast by adding the boolean attribute `toast`.

Example with a single warning message:

```html
<cells-modal-alert content="Warning message sample"></cells-modal-alert>
```

The `content` property allows HTML tags that can be used for basic text formatting (bold, italics, etc.).

```html
<cells-modal-alert content="Warning <i>message</i> sample"></cells-modal-alert>
```

Example with content in light DOM:

```html
<cells-modal-alert>
  <h2 class="heading">Lorem ipsum dolor sit amet</h2>
  <p>Lorem ipsum dolor sit amet...</p>
</cells-modal-alert>
```

## Toast mode

When the modal is used as a toast, the `type` attribute establishes the background color.   
Possible values for the `type` attribute in toast mode are: `error`, `info`, `warning` and `success`.   
The following example will display an error toast with red background (default styles):

```html
<cells-modal-alert toast
  type="error"
  content="Network connection lost">
</cells-modal-alert>
```

By default the toast is closed after 6 seconds. The timeout is set in the `duration` property.   
To **keep the toast on screen**, set `duration` property to 0:

```html
<cells-modal-alert toast
  duration="0"
  type="error"
  content="Network connection lost">
</cells-modal-alert>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-modal-alert | mixin applied to the :host | {} |
| --cells-modal-alert-color | Default text color | #121212 |
| --cells-modal-alert-bg-color | Default background color | #fff |
| --cells-modal-alert-footer-buttons | mixin applied to the buttons container | {} |
| --cells-modal-alert-footer-button | mixin applied to each button | {} |
| --cells-modal-alert-toast-color | text color in toast mode | #fff |
| --cells-modal-alert-error-color | text color for error toasts | #fff |
| --cells-modal-alert-success-color | text color for success toasts | #fff |
| --cells-modal-alert-info-color | text color for info toasts | #fff |
| --cells-modal-alert-warning-color | text color for warning toasts | #fff |
| --cells-modal-alert-toast-error-bg-color | background color for error toasts | #B92A45 |
| --cells-modal-alert-toast-success-bg-color | background color for success toasts | #48AE64 |
| --cells-modal-alert-toast-info-bg-color | background color for info toasts | #1464A5 |
| --cells-modal-alert-toast-warning-bg-color | background color for warning toasts | #d8732c |
