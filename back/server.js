
var express = require('express')
var bodyParser = require('body-parser')
var app = express()
var requestJson = require('request-json')
app.use(bodyParser.json())

var port = process.env.PORT || 3000
var fs = require('fs')

app.listen(port)


console.log("API escuchando en el puerto" + port)

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancotgg/collections"
var apiKey = "apiKey=dy1jqe_12EhsQOwGudqTK10ssXl2YDlz"
var clienteMlab = null

//-------------GESTION DE USUARIOS-------------------------------------//

// alta de usuarios headers

app.post('/usuarios', function(req, res) {
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body){
    if (!err){
      var contador = body.length +1
      var logado = false
      var nuevousuario =
       '{' +
                 '"id":' + contador + ',' +
                 '"nombre":' + '"'+ req.headers.nombre +'"' + ',' +
                 '"apellido":' + '"'+ req.headers.apellido +'"' + ',' +
                 '"ciudad":' + '"'+ req.headers.ciudad +'"' + ',' +
                 '"telefono":' + '"'+ req.headers.telefono +'"' + ',' +
                 '"email":' + '"'+ req.headers.email +'"' + ',' +
                 '"password":' + '"'+ req.headers.password +'"' + ',' +
                 '"logged":' + logado +
        '}'
      clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
      clienteMlab.post('', JSON.parse(nuevousuario), function(err, resP, bodyP){
        if (!err){
          res.send(body)
        }else{
          res.send("Ko")
        }
      })
    }
  })
})

//obtener todos los usuarios

app.get('/usuarios', function(req, res){
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body){
    if (!err){
      res.send(body)
    }
  })
})

// dado un id saca toda la infomacion de ese id

app.get('/usuarios/:id', function(req, res) {
  var id = req.params.id
  var query = 'q={"id":' + id + '}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0)
        res.send(body[0])
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

// hacer login con un email y password y añadir el atributo logado al registro encontrado

app.post('/usuarios/login', function(req, res) {
    var email = req.headers.email
    var password = req.headers.password
    var query = 'q={"email":"' + email + '","password":"' + password + '"}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) // Login ok
        {
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":true}}'
          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"login":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellidos":body[0].apellidos})
          })

        }
        else {
          res.status(404).send('Usuario no encontrado')
        }
      }
    })
})

//hacer el logout de usuarios
app.post('/usuarios/logout', function(req, res) {
    var id = req.headers.id
    var query = 'q={"id":' + id + ', "logged":true}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) // Cliente estaba logado
        {
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":false}}'
          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"logout":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellidos":body[0].apellidos})
          })

        }
        else {
          res.status(404).send('Usuario no logado previamente')
        }
      }
    })
})

//------------- GESTION DE CONTRATOS -------------------------------

// consultar los contratos de un cliente (con estado en mlab)

app.get('/contratos/:idcliente', function(req, res) {
    var estado =  '"'+ req.headers.estado +'"'
    var query = 'q={"idcliente":' + req.params.idcliente + ', "estado":' + estado +'}'
    var filter = 'f={"iban":1,"saldo":2,"_id":0}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/Contratos?" + query + "&" + filter + "&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
    if (!err) {
        if (body.length > 0) // Cliente encontrado
        {
          res.send(body)
        }
        else {
          res.status(404).send("identificador no encontrado")
        }
      }
    })
})

//hacer la cancelacion de un contrato
app.post('/contratos/cancelar/:iban', function(req, res) {
    var iban = '"'+ req.params.iban +'"'
    var query = 'q={"iban":' + iban + ', "estado": "A"}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/Contratos?" + query + "&l=1&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) // El contrato estaba activo
        {
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/Contratos")
          var cambio = '{"$set":{"estado":"C"}}'
          var iban2 = '"'+ body[0].iban +'"'
          clienteMlab.put('?q={"iban":' + iban2 + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"idcliente":body[0].idcliente, "iban":body[0].iban, "saldo":body[0].saldo, "estado":body[0].estado})
          })

        }
        else {
          res.status(404).send('Contrato no consultado previamente')
        }
      }
    })
})

//alta de un contrato para un cliente

app.post('/contratos', function(req, res) {
    var idcliente = req.headers.idcliente
    var saldo = '"0,00€"'
    var estado = '"A"'

    var fecha = new Date().toJSON().slice(0,10)

    var fechaformateada = fecha.substring(8,10) + "/"
                        + fecha.substring(5,7) + "/"
                        + fecha.substring(0,4);
    var aa = fecha.substring(0,2)
    var mes = fecha.substring(5,7)
    var dia = fecha.substring(8,10)
    var tiempo = new Date()
    var hora = tiempo.getHours()
    var minuto = tiempo.getMinutes()
    var segundo = tiempo.getSeconds()
    var bbva = "ES01 0182"
    var restocontrato1 = '"'+ aa + mes +'"'
    var restocontrato2 = '"'+ dia + hora +'"'
    var restocontrato3 = '"'+ minuto + segundo +'"'

    var iban = '"'+ bbva + " " + aa + mes + " " + dia + hora + " " + minuto + segundo +'"'

    var nuevocontrato  =
     '{' +
             '"idcliente":' + idcliente + ',' +
             '"iban":' + iban + ',' +
             '"saldo":' + saldo + ',' +
             '"estado":' + estado +
     '}'

    clienteMlab = requestJson.createClient(urlMlabRaiz + "/Contratos?" + apiKey)
    clienteMlab.post('', JSON.parse(nuevocontrato), function(err, resP, bodyP){
    if (err){
      res.send("KO")
    }else{
      var nummov = 1
      var importe = '"0,00€"'
      var concepto = '"Apertura"'
      var nuevomovimiento  =
       '{' +
               '"iban":' + iban + ',' +
               '"movimientos":[' +
                       '{' +
                           '"id":' + nummov + ',' +
                           '"fecha":' + '"' + fechaformateada + '"' + ',' +
                           '"concepto":' + concepto + ',' +
                           '"importe":' + importe +
                       '}' +
               ']' +
       '}'

       clienteMlab = requestJson.createClient(urlMlabRaiz + "/Movimientos?" + "&" + apiKey)
       clienteMlab.post('', JSON.parse(nuevomovimiento), function(err, resP, bodyP) {

       if (!err){
         res.send(bodyP)
       }else{
         res.send("KO")
       }

       })
    }
  })
})

//------------------ GESTION DE MOVIMIENTOS ----------------------------

// obtener los movimientos de un contrato

app.get('/contratos/movimientos/:iban', function(req, res) {
  var query = 'q={"iban":"' + req.params.iban + '"}'
  var filter = 'f={"movimientos":1,"_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Movimientos?" + query + "&" + filter + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
  if (!err) {
    if (body.length > 0) // Cliente encontrado
      {
        res.send(body)
      }
      else {
        res.status(404).send("identificador no encontrado")
      }
    }
  })
})

//cancelar un contrato a traves de Movimientos

app.delete('/contratos', function(req, res){
  var iban = '"'+ req.headers.iban +'"'
  var query = 'q={"iban":' + iban + '}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Movimientos?" + query + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
  if (!err) {
      if (body.length > 0) // Cliente encontrado
      {
        var iddocumento = body[0]._id.$oid
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/Movimientos/" +  iddocumento + "?" + apiKey)
        clienteMlab.delete('', function(err, resM, body){
          if (!err) {
            res.send(body)
          }else{
            res.status(404).send("identificador no encontrado")
          }
        })
      }
      else {
        res.status(404).send("identificador no encontrado")
      }
    }
  })
})
